package com.societe.training;

public abstract class Details {
	double balance;

	

	public Details(double balance) {
		super();
		this.balance = balance;
	}
	abstract void withdraw(double amount);
//	{
//		
//		System.out.println(balance);
//	}
	abstract void deposit(double amount);
//	{
//		
//		System.out.println(balance);
//	}
	double getBalance()
	{
		return balance;
		
	}
	
	
}

class ATMMain{
	public static void main(String[] args) {
		Details a=new Savings(5000.00);
		a.withdraw(2000.00);
		a.deposit(500.00);
		a.getBalance();
		
		Details b=new Current(1000.00);
		b.deposit(500);
		b.withdraw(1000.00);
		b.getBalance();
		
		Current current= (Current) b;
		current.checkInterest();
	
		
	}
}
