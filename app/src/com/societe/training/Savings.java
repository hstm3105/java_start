package com.societe.training;

public class Savings extends Details {

	public Savings(double balance) {
		super(balance);
		// TODO Auto-generated constructor stub
	}

	@Override
	void withdraw(double amount) {
		balance=balance-amount;
		System.out.println(balance);
	}

	@Override
	void deposit(double amount) {
		balance=balance+amount;
		System.out.println(balance);
	}

}
