package com.societe.training;

public class Current extends Details {

	@Override
	void withdraw(double amount) {
		balance=balance-amount;
		System.out.println(balance);
	}

	@Override
	void deposit(double amount) {
		balance=balance+amount;
		System.out.println(balance);
	}
	void checkInterest() {
		System.out.println("checking interest");
	}

	public Current(double balance) {
		super(balance);
	}

}
