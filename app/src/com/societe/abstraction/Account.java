package com.societe.abstraction;

public abstract class Account {

	abstract void carLoan();
	abstract void housingLoan();
	abstract void EduLoan();
	
	final void adminDetails() {
		System.out.println("bank admin details");
	}
}
