package com.societe.abstraction;

public class Main {

	public static void main(String[] args) {
		Account ref;
		ref= new Branch1();
		ref.carLoan();
		ref.housingLoan();
		ref.adminDetails();
		Branch1 br1=(Branch1)ref;
		br1.br1Method();
		
		ref= new Subbranch();
		ref.carLoan();
		ref.housingLoan();
		ref.adminDetails();
		
		Subbranch sub=(Subbranch)ref;
		sub.br2Method();
		sub.subPay();
		
		Branch2 brref=(Branch2)ref;
		brref.br2Method();
	}

}
