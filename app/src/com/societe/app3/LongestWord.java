package com.societe.app3;
import java.util.Scanner;

public class LongestWord {
	
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		int count=0;
		while(s.nextLine()!=null)
		{
			String str=s.nextLine();
			if(count<str.length())
				count=str.length();
		}
		System.out.println(count);
	}
}
